# Calls

A phone dialer and call handler.

As of 2021-07-12 Upstream development moved to *GNOME*. The new location for code
and issues is at https://gitlab.gnome.org/GNOME/calls.

The packaging for the *Librem 5* and *PureOS*
lives at https://source.puri.sm/Librem5/debs/pkg-calls.
